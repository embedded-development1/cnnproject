import numpy as np
import cv2 as cv

img = cv.imread('alphabet.png')
#gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
# Now we split the image to 350 cells, each 20x20 size
cells = [np.hsplit(row,50) for row in np.vsplit(img,7)]
# Make it into a Numpy array: its size will be (7,50,20,20)
x = np.array(cells)
print(x.shape)
# Now we prepare the training data and test data
train = x[:,:25].reshape(175, 20, 20, -1)
test = x[:,25:50].reshape(175, 20, 20, -1)
print(train.shape)
#print(train)
print(test.shape)
# Create labels for train and test data
k = np.arange(7)
train_labels = np.repeat(k,25)[:,np.newaxis]
test_labels = train_labels.copy()
print(train_labels.shape)

np.savez('alphabet_data.npz',train=train, train_labels=train_labels, test=test, test_labels=test_labels)