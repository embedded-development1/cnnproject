# Alphabet recognition using CNN
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Background](#background)
- [Implementation](#implementation)
- [Requirements](#requirements)
- [Usage](#usage)
- [API](#api)
- [Contributing](#contributing)
- [License](#license)

## Background
[Convolutional neural network:](https://en.wikipedia.org/wiki/Convolutional_neural_network) A class of artificial neural networks that are a specialized type of artificial neural networks that use a mathematical operation called convolution in place of general matrix multiplication in at least one of their layers. They are specifically designed to process pixel data and are used in image recognition and processing. 

The neural network uses convolutional layers that convolve the input and pass its result to the next layer. This type of layer is like small square templates that slide over the image and look for patterns, returning positive in a match and zero or negative for no match.

Convolutional networks may include local and/or global pooling layers along with traditional convolutional layers. Pooling layers reduce the dimensions of data by combining the outputs of neuron clusters at one layer into a single neuron in the next layer

### Assignment
"Instructions: 

    - Use your previously generated handwriting dataset.
    - Apply another type of ML on that. Use CNN. Take a look at the slides, the documentation, and the sample code as well here: https://www.tensorflow.org/tutorials/images/cnn
    - Check what the accuracy is with the default layer sizes and numbers.
    - Try adding more (removing) layers, change the number of neurons at hidden layers, and increase/decrease the epochs used in "model.fit()". Do you see any difference in accuracy? Report these numbers on a plot using matplotlib. Take a look at the documentation if needed at https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html

## Implementation
The program uses the Keras Sequential API that is part of the Tensorflow Api to create and train the model, numpy to create and handle the dataset, matplotlib to print out the graph and opencv to load the image. 

When you want to create data, the image is loaded and divided into cells that are then divided into train and test data. Train and testlabels are also created and is the saved. 

When the CNN is being runned, the dataset is loaded and the CNN model is created and trained with the dataset and then printed out after how accurate it is and how many iterations.

The highest accuracy was 98.3%

Some results:

![alt picture](Images/Default.PNG)
Default values (1 layer, 64 neurons, 10 epoch)
![alt picture](Images/1Layer_32neuron_epoch10.PNG)
1 layer, 32 neurons, 10 epoch
![alt picture](Images/2Layer_64_32neuron_epoch10.PNG)
2 layers, 64 and 32 neurons, 10 epoch
## Requirements
* [Python](https://www.python.org/downloads/)
* [Numpy](https://numpy.org/)
* [Tensorflow](https://www.tensorflow.org/)
* [Opencv](https://opencv.org/)
* [matplotlib](https://matplotlib.org/)

Required :
```
- Install Python https://www.python.org/downloads/
(On linux type sudo apt-get install python3.8 python3-pip in console)
Open command window or console and type the following:
- pip install opencv-python
- pip install numpy
- pip install matplotlib
- pip install tensorflow
```
Install the program:
```
- Clone the repo or install the zip file.
- Install the API from requirements
- Run the commands under usage
```

## Usage

```
- py CNN.py
```
The program will use the included dataset to build a neural network model. Default is with one hidden layer with 64 neurons and 10 epochs. A history of the training and a evaluation score will be printed. Images are included to give an idea on how different number of layers, neurons and epochs changes the training and the result.
```
- py CreateData.py
```
This is used to create dataset of the included image. The image is of letters A-G and each letter is 20x20 and contains 7 rows with 50 letters each row. If you want to use your own image change these lines:
```
- img = cv.imread('name of image')

- cells = [np.hsplit(row,"number of columns") for row in np.vsplit(img,"number of rows")]

- train = x[:,:"Images per row divided by how you want it divided (50% of 50 becomes 25)"].reshape("Total amount of images in training dataset(25 times 7 equals 175)", "size of each cell in height", "size of each cell in width", -1)

- test = x[:,"Start from train dataset (25)":"Number of images per row (50)"].reshape("Total amount of images in test dataset(25 times 7 equals 175)", "size of each cell in height", "size of each cell in width", -1)

- k = np.arange("number of rows")

- train_labels = np.repeat(k,"number of train letters")[:,np.newaxis]
```


## API
- [Numpy](https://numpy.org/)
- [Tensorflow](https://www.tensorflow.org/)
- [Opencv](https://opencv.org/)
- [matplotlib](https://matplotlib.org/)

## Contributing
You are welcome to clone the project and make small changes to the program.
You can also fork the project and add new features if you give credit to the author. 
Bigger changes should have an issue opened to discuss what changes you want to do.

## License
[MIT](docs/LICENSE.md)

